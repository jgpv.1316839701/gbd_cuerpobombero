
/*==============================================================*/
/* Table: ASCENSO                                               */
/*==============================================================*/
create table ASCENSO (
   ASCENSO_ID           SERIAL               not null,
   BOMBERO_ID           INT4                 not null,
   RANGO_ID             INT4                 not null,
   ASCENSO_FECHA        DATE                 not null,
   constraint PK_ASCENSO primary key (ASCENSO_ID)
);

/*==============================================================*/
/* Index: ASCENSO_PK                                            */
/*==============================================================*/
create unique index ASCENSO_PK on ASCENSO (
ASCENSO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_16_FK on ASCENSO (
BOMBERO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_17_FK on ASCENSO (
RANGO_ID
);

/*==============================================================*/
/* Table: ASIGNACION                                            */
/*==============================================================*/
create table ASIGNACION (
   ASIGNACION_ID        SERIAL               not null,
   EQUIPOS_ID           INT4                 not null,
   BOMBERO_ID           INT4                 not null,
   ASIGNACION_CANTIDAD  NUMERIC              not null,
   ASIGNACION_FECHA     DATE                 not null,
   ASIGNACION_ESTADO    TEXT                 not null,
   constraint PK_ASIGNACION primary key (ASIGNACION_ID)
);


/*==============================================================*/
/* Index: ASIGNACION_PK                                         */
/*==============================================================*/
create unique index ASIGNACION_PK on ASIGNACION (
ASIGNACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_22_FK on ASIGNACION (
EQUIPOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_23_FK on ASIGNACION (
BOMBERO_ID
);

/*==============================================================*/
/* Table: BOMBERO                                               */
/*==============================================================*/
create table BOMBERO (
   BOMBERO_ID           SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   BOMBERO_NOMBRE       TEXT                 not null,
   BOMBERO_APELLIDO     TEXT                 not null,
   BOMBERO_CEDULA       NUMERIC(10)          not null,
   BOMBERO_TELEFONO     NUMERIC(10)          not null,
   BOMBERO_DIRECCION    TEXT                 not null,
   BOMBERO_FECHA_CONTRATO DATE                 not null,
   BOMBERO_TIPO_SANGRE  TEXT                 not null,
   constraint PK_BOMBERO primary key (BOMBERO_ID)
);

/*==============================================================*/
/* Index: BOMBERO_PK                                            */
/*==============================================================*/
create unique index BOMBERO_PK on BOMBERO (
BOMBERO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on BOMBERO (
DISTRITO_ID
);

/*==============================================================*/
/* Table: CANTON_UBICACION                                      */
/*==============================================================*/
create table CANTON_UBICACION (
   CANTON_ID            SERIAL               not null,
   CANTON_NOMBRE        TEXT                 null,
   constraint PK_CANTON_UBICACION primary key (CANTON_ID)
);

/*==============================================================*/
/* Index: CANTON_UBICACION_PK                                   */
/*==============================================================*/
create unique index CANTON_UBICACION_PK on CANTON_UBICACION (
CANTON_ID
);

/*==============================================================*/
/* Table: CAPACITACION                                          */
/*==============================================================*/
create table CAPACITACION (
   CAPACITACION_ID      SERIAL               not null,
   BOMBERO_ID           INT4                 not null,
   CAPACITACION_NOMBRE  TEXT                 not null,
   CAPACITACION_FECHA   DATE                 not null,
   CAPACITACION_NOTA    NUMERIC(2)           not null,
   constraint PK_CAPACITACION primary key (CAPACITACION_ID)
);

/*==============================================================*/
/* Index: CAPACITACION_PK                                       */
/*==============================================================*/
create unique index CAPACITACION_PK on CAPACITACION (
CAPACITACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_27_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_27_FK on CAPACITACION (
BOMBERO_ID
);

/*==============================================================*/
/* Table: DEVOLUCION                                            */
/*==============================================================*/
create table DEVOLUCION (
   DEVOLUCION_ID        SERIAL               not null,
   ASIGNACION_ID        INT4                 not null,
   DEVOLUCION_FECHA     DATE                 not null,
   DEVOLUCION_CANTIDAD  NUMERIC              not null,
   constraint PK_DEVOLUCION primary key (DEVOLUCION_ID)
);

/*==============================================================*/
/* Index: DEVOLUCION_PK                                         */
/*==============================================================*/
create unique index DEVOLUCION_PK on DEVOLUCION (
DEVOLUCION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_28_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_28_FK on DEVOLUCION (
ASIGNACION_ID
);

/*==============================================================*/
/* Table: DISTRITO                                              */
/*==============================================================*/
create table DISTRITO (
   DISTRITO_ID          SERIAL               not null,
   DISTRITO_CANTON      TEXT                 null,
   DISTRITO_SECTOR      TEXT                 null,
   constraint PK_DISTRITO primary key (DISTRITO_ID)
);

/*==============================================================*/
/* Index: DISTRITO_PK                                           */
/*==============================================================*/
create unique index DISTRITO_PK on DISTRITO (
DISTRITO_ID
);

/*==============================================================*/
/* Table: EQUIPOS                                               */
/*==============================================================*/
create table EQUIPOS (
   EQUIPOS_ID           SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   EQUIPOS_NOMBRE       TEXT                 null,
   EQUIPOS_CANTIDAD     NUMERIC              null,
   constraint PK_EQUIPOS primary key (EQUIPOS_ID)
);

/*==============================================================*/
/* Index: EQUIPOS_PK                                            */
/*==============================================================*/
create unique index EQUIPOS_PK on EQUIPOS (
EQUIPOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_21_FK on EQUIPOS (
DISTRITO_ID
);

/*==============================================================*/
/* Table: INCIDENTE                                             */
/*==============================================================*/
create table INCIDENTE (
   INCIDENTE_ID         SERIAL               not null,
   UBICACION_ID         INT4                 not null,
   NIVEL_PELIGRO_ID     INT4                 not null,
   TIPO_INDICENTE_ID    INT4                 not null,
   REPORTADOR_ID        INT4                 not null,
   DISTRITO_ID          INT4                 not null,
   INCIDENTE_FECHA      DATE                 null,
   INCIDENTE_DESCRIPCION TEXT                 null,
   constraint PK_INCIDENTE primary key (INCIDENTE_ID)
);

/*==============================================================*/
/* Index: INCIDENTE_PK                                          */
/*==============================================================*/
create unique index INCIDENTE_PK on INCIDENTE (
INCIDENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on INCIDENTE (
UBICACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on INCIDENTE (
NIVEL_PELIGRO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on INCIDENTE (
TIPO_INDICENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on INCIDENTE (
REPORTADOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on INCIDENTE (
DISTRITO_ID
);

/*==============================================================*/
/* Table: INGRESO_EQUIPOS                                       */
/*==============================================================*/
create table INGRESO_EQUIPOS (
   INGRESO_ID           SERIAL               not null,
   ORIGEN_ID            INT4                 not null,
   EQUIPOS_ID           INT4                 not null,
   INGRESO_CANTIDAD     NUMERIC              null,
   INGRESO_FECHA        DATE                 null,
   constraint PK_INGRESO_EQUIPOS primary key (INGRESO_ID)
);

/*==============================================================*/
/* Index: INGRESO_EQUIPOS_PK                                    */
/*==============================================================*/
create unique index INGRESO_EQUIPOS_PK on INGRESO_EQUIPOS (
INGRESO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_20_FK on INGRESO_EQUIPOS (
ORIGEN_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_26_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_26_FK on INGRESO_EQUIPOS (
EQUIPOS_ID
);

/*==============================================================*/
/* Table: MANTENIMIENTO_VEHICULO                                */
/*==============================================================*/
create table MANTENIMIENTO_VEHICULO (
   MANTENIMIENTO_ID     SERIAL               not null,
   VEHICULOS_ID         INT4                 not null,
   MANTENIMIENTO_FECHA  DATE                 null,
   MANTENIMIENTO_GASTO  MONEY                null,
   constraint PK_MANTENIMIENTO_VEHICULO primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Index: MANTENIMIENTO_VEHICULO_PK                             */
/*==============================================================*/
create unique index MANTENIMIENTO_VEHICULO_PK on MANTENIMIENTO_VEHICULO (
MANTENIMIENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_15_FK on MANTENIMIENTO_VEHICULO (
VEHICULOS_ID
);

/*==============================================================*/
/* Table: NIVEL_PELIGRO                                         */
/*==============================================================*/
create table NIVEL_PELIGRO (
   NIVEL_PELIGRO_ID     SERIAL               not null,
   NIVEL_PELIGRO_NOMBRE TEXT                 null,
   constraint PK_NIVEL_PELIGRO primary key (NIVEL_PELIGRO_ID)
);

/*==============================================================*/
/* Index: NIVEL_PELIGRO_PK                                      */
/*==============================================================*/
create unique index NIVEL_PELIGRO_PK on NIVEL_PELIGRO (
NIVEL_PELIGRO_ID
);

/*==============================================================*/
/* Table: ORIGEN_EQUIPOS                                        */
/*==============================================================*/
create table ORIGEN_EQUIPOS (
   ORIGEN_ID            SERIAL               not null,
   ORIGEN_NOMBRE        TEXT                 null,
   constraint PK_ORIGEN_EQUIPOS primary key (ORIGEN_ID)
);

/*==============================================================*/
/* Index: ORIGEN_EQUIPOS_PK                                     */
/*==============================================================*/
create unique index ORIGEN_EQUIPOS_PK on ORIGEN_EQUIPOS (
ORIGEN_ID
);

/*==============================================================*/
/* Table: PRUEBA_NOTA                                           */
/*==============================================================*/
create table PRUEBA_NOTA (
   PRUEBA_ID            SERIAL               not null,
   ASCENSO_ID           INT4                 not null,
   PRUEBA_ACADEMICA     NUMERIC(2)           not null,
   PRUEBA_CONFIANZA     NUMERIC(2)           not null,
   PRUEBA_PSICOMETRICAS NUMERIC(2)           not null,
   PRUEBA_FISICAS       NUMERIC(2)           not null,
   PRUEBA_ENTREVISTA    NUMERIC(2)           not null,
   constraint PK_PRUEBA_NOTA primary key (PRUEBA_ID)
);

/*==============================================================*/
/* Index: PRUEBA_NOTA_PK                                        */
/*==============================================================*/
create unique index PRUEBA_NOTA_PK on PRUEBA_NOTA (
PRUEBA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_18_FK on PRUEBA_NOTA (
ASCENSO_ID
);

/*==============================================================*/
/* Table: RANGO                                                 */
/*==============================================================*/
create table RANGO (
   RANGO_ID             SERIAL               not null,
   RANGO_NOMBRE         TEXT                 null,
   RANGO_ANOS_NECESARIOS NUMERIC(2)           null,
   constraint PK_RANGO primary key (RANGO_ID)
);

/*==============================================================*/
/* Index: RANGO_PK                                              */
/*==============================================================*/
create unique index RANGO_PK on RANGO (
RANGO_ID
);

/*==============================================================*/
/* Table: REPORTADOR                                            */
/*==============================================================*/
create table REPORTADOR (
   REPORTADOR_ID        SERIAL               not null,
   REPORTADOR_NOMBRE    TEXT                 null,
   REPORTADOR_APELLIDO  TEXT                 null,
   REPORTADOR_FECHA     DATE                 null,
   constraint PK_REPORTADOR primary key (REPORTADOR_ID)
);

/*==============================================================*/
/* Index: REPORTADOR_PK                                         */
/*==============================================================*/
create unique index REPORTADOR_PK on REPORTADOR (
REPORTADOR_ID
);

/*==============================================================*/
/* Table: SECTOR_UBICACION                                      */
/*==============================================================*/
create table SECTOR_UBICACION (
   SECTOR_ID            SERIAL               not null,
   SECTOR_NOMBRE        TEXT                 null,
   constraint PK_SECTOR_UBICACION primary key (SECTOR_ID)
);

/*==============================================================*/
/* Index: SECTOR_UBICACION_PK                                   */
/*==============================================================*/
create unique index SECTOR_UBICACION_PK on SECTOR_UBICACION (
SECTOR_ID
);

/*==============================================================*/
/* Table: TIPO_INDICENTE                                        */
/*==============================================================*/
create table TIPO_INDICENTE (
   TIPO_INDICENTE_ID    SERIAL               not null,
   TIPO_INDICENTE_NOMBRE TEXT                 null,
   constraint PK_TIPO_INDICENTE primary key (TIPO_INDICENTE_ID)
);

/*==============================================================*/
/* Index: TIPO_INDICENTE_PK                                     */
/*==============================================================*/
create unique index TIPO_INDICENTE_PK on TIPO_INDICENTE (
TIPO_INDICENTE_ID
);

/*==============================================================*/
/* Table: UBICACION                                             */
/*==============================================================*/
create table UBICACION (
   UBICACION_ID         SERIAL               not null,
   SECTOR_ID            INT4                 not null,
   CANTON_ID            INT4                 not null,
   UBICACION_DIRECCION  TEXT                 null,
   UBICACION_REFERENCIA TEXT                 null,
   constraint PK_UBICACION primary key (UBICACION_ID)
);

/*==============================================================*/
/* Index: UBICACION_PK                                          */
/*==============================================================*/
create unique index UBICACION_PK on UBICACION (
UBICACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on UBICACION (
SECTOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on UBICACION (
CANTON_ID
);

/*==============================================================*/
/* Table: VEHICULOS                                             */
/*==============================================================*/
create table VEHICULOS (
   VEHICULOS_ID         SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   BOMBERO_ID           INT4                 not null,
   VEHICULOS_PLACA      TEXT                 null,
   VEHICULOS_OPERATIVO  TEXT                 null,
   constraint PK_VEHICULOS primary key (VEHICULOS_ID)
);

/*==============================================================*/
/* Index: VEHICULOS_PK                                          */
/*==============================================================*/
create unique index VEHICULOS_PK on VEHICULOS (
VEHICULOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_25_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_25_FK on VEHICULOS (
DISTRITO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_24_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_24_FK on VEHICULOS (
BOMBERO_ID
);



alter table ASCENSO
   add constraint FK_ASCENSO_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table ASCENSO
   add constraint FK_ASCENSO_RELATIONS_RANGO foreign key (RANGO_ID)
      references RANGO (RANGO_ID)
      on delete restrict on update restrict;

--alter table ASIGNACION ADD CHECK (ASIGNACION_CANTIDAD>'0');
alter table ASIGNACION
   add constraint FK_ASIGNACI_RELATIONS_EQUIPOS foreign key (EQUIPOS_ID)
      references EQUIPOS (EQUIPOS_ID)
      on delete restrict on update restrict;

alter table ASIGNACION
   add constraint FK_ASIGNACI_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table BOMBERO
   add constraint FK_BOMBERO_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table CAPACITACION
   add constraint FK_CAPACITA_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

--alter table DEVOLUCION ADD CHECK (DEVOLUCION_CANTIDAD>'0');
alter table DEVOLUCION
   add constraint FK_DEVOLUCI_RELATIONS_ASIGNACI foreign key (ASIGNACION_ID)
      references ASIGNACION (ASIGNACION_ID)
      on delete restrict on update restrict;

alter table EQUIPOS
   add constraint FK_EQUIPOS_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_UBICACIO foreign key (UBICACION_ID)
      references UBICACION (UBICACION_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_NIVEL_PE foreign key (NIVEL_PELIGRO_ID)
      references NIVEL_PELIGRO (NIVEL_PELIGRO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_TIPO_IND foreign key (TIPO_INDICENTE_ID)
      references TIPO_INDICENTE (TIPO_INDICENTE_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_REPORTAD foreign key (REPORTADOR_ID)
      references REPORTADOR (REPORTADOR_ID)
      on delete restrict on update restrict;

alter table INGRESO_EQUIPOS 
   add constraint FK_INGRESO__RELATIONS_ORIGEN_E foreign key (ORIGEN_ID)
      references ORIGEN_EQUIPOS (ORIGEN_ID)
      on delete restrict on update restrict;
	  
--alter table INGRESO_EQUIPOS ADD CHECK (INGRESO_CANTIDAD>'0');
alter table INGRESO_EQUIPOS 
   add constraint FK_INGRESO__RELATIONS_EQUIPOS foreign key (EQUIPOS_ID)
      references EQUIPOS (EQUIPOS_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO_VEHICULO
   add constraint FK_MANTENIM_RELATIONS_VEHICULO foreign key (VEHICULOS_ID)
      references VEHICULOS (VEHICULOS_ID)
      on delete restrict on update restrict;

alter table PRUEBA_NOTA
   add constraint FK_PRUEBA_N_RELATIONS_ASCENSO foreign key (ASCENSO_ID)
      references ASCENSO (ASCENSO_ID)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_RELATIONS_SECTOR_U foreign key (SECTOR_ID)
      references SECTOR_UBICACION (SECTOR_ID)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_RELATIONS_CANTON_U foreign key (CANTON_ID)
      references CANTON_UBICACION (CANTON_ID)
      on delete restrict on update restrict;

alter table VEHICULOS
   add constraint FK_VEHICULO_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table VEHICULOS
   add constraint FK_VEHICULO_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;







/* INSERTAR DATOS: DISTRITO                          */
INSERT INTO DISTRITO VALUES('13021','Manta','Centro');
INSERT INTO DISTRITO VALUES('13022','Jaramijo','Anibal San Andres');
INSERT INTO DISTRITO VALUES('13023','Montecristi','Parque Central Montecristi');

/* INSERTAR DATOS: RANGO                          */
INSERT INTO RANGO VALUES('01','Bombero','4');
INSERT INTO RANGO VALUES('02','Cabo','4');
INSERT INTO RANGO VALUES('03','Sargento','4');
INSERT INTO RANGO VALUES('04','Suboficial','4');
INSERT INTO RANGO VALUES('05','Subteniente','4');
INSERT INTO RANGO VALUES('06','Teniente','4');
INSERT INTO RANGO VALUES('07','Capitan','4');
INSERT INTO RANGO VALUES('08','Mayor','4');
INSERT INTO RANGO VALUES('09','Teniente Coronel','4');
INSERT INTO RANGO VALUES('10','Coronel','4');

/* INSERTAR DATOS: TIPO_INDICENTE                          */
INSERT INTO TIPO_INDICENTE VALUES('2001','Accidentado');
INSERT INTO TIPO_INDICENTE VALUES('2002','Vehiculo Accidentado');
INSERT INTO TIPO_INDICENTE VALUES('2003','Explosivo');
INSERT INTO TIPO_INDICENTE VALUES('2004','Atropellado');
INSERT INTO TIPO_INDICENTE VALUES('2005','Inundacion');
INSERT INTO TIPO_INDICENTE VALUES('2006','Incendio Vehicular');
INSERT INTO TIPO_INDICENTE VALUES('2007','Derrame Producto');
INSERT INTO TIPO_INDICENTE VALUES('2008','Derrumbe');
INSERT INTO TIPO_INDICENTE VALUES('2009','Fuga de gas');
INSERT INTO TIPO_INDICENTE VALUES('2010','incendio');
INSERT INTO TIPO_INDICENTE VALUES('2011','incendio Forestal');
INSERT INTO TIPO_INDICENTE VALUES('2012','Rescate');
INSERT INTO TIPO_INDICENTE VALUES('2013','Corto Circuito');
INSERT INTO TIPO_INDICENTE VALUES('2014','Evacuacion');
INSERT INTO TIPO_INDICENTE VALUES('2015','Poste Electrico');

/* INSERTAR DATOS: NIVEL_PELIGRO                          */
INSERT INTO NIVEL_PELIGRO VALUES('1','Urgencia');
INSERT INTO NIVEL_PELIGRO VALUES('2','Emergencia');
INSERT INTO NIVEL_PELIGRO VALUES('3','Desastre');
INSERT INTO NIVEL_PELIGRO VALUES('4','Catastrofe');

/* INSERTAR DATOS: REPORTADOR                          */
INSERT INTO REPORTADOR VALUES('1001','Jose','Intriago','2020-02-16');
INSERT INTO REPORTADOR VALUES('1002','Maria','España','2020-05-24');
INSERT INTO REPORTADOR VALUES('1003','Gabriel','Pico','2020-06-02');
INSERT INTO REPORTADOR VALUES('1004','Jose','Sanches','2020-06-02');
INSERT INTO REPORTADOR VALUES('1005','Jona','Reyes','2020-06-02');
INSERT INTO REPORTADOR VALUES('1006','Gabriel','España','2020-06-02');
INSERT INTO REPORTADOR VALUES('1007','Emilia','Pico','2020-06-02');
INSERT INTO REPORTADOR VALUES('1008','Laura','Lopez','2020-06-02');
INSERT INTO REPORTADOR VALUES('1009','Bryan','Pico','2020-06-02');
INSERT INTO REPORTADOR VALUES('1010','Joffre','Vargas','2020-06-02');

INSERT INTO REPORTADOR VALUES('1011','Jose','Intriago','2021-02-16');
INSERT INTO REPORTADOR VALUES('1012','Maria','España','2021-05-24');
INSERT INTO REPORTADOR VALUES('1013','Gabriel','Pico','2021-06-02');
INSERT INTO REPORTADOR VALUES('1014','Jose','Sanches','2021-06-02');
INSERT INTO REPORTADOR VALUES('1015','Jona','Reyes','2021-06-02');
INSERT INTO REPORTADOR VALUES('1016','Gabriel','España','2021-06-02');
INSERT INTO REPORTADOR VALUES('1017','Emilia','Pico','2021-06-02');

/* INSERTAR DATOS: SECTOR_UBICACION  */
INSERT INTO SECTOR_UBICACION VALUES('001','Tarqui');
INSERT INTO SECTOR_UBICACION VALUES('002','Los Esteros');
INSERT INTO SECTOR_UBICACION VALUES('003','San Mateo');
INSERT INTO SECTOR_UBICACION VALUES('004','Eloy Alfaro');
INSERT INTO SECTOR_UBICACION VALUES('005','Centro Manta');
INSERT INTO SECTOR_UBICACION VALUES('006','Anibal San Andres');
INSERT INTO SECTOR_UBICACION VALUES('007','Ciudad Alfaro');
INSERT INTO SECTOR_UBICACION VALUES('008','Puerto Pesquero');

/* INSERTAR DATOS: CANTON_UBICACION                          */
INSERT INTO CANTON_UBICACION VALUES('001','Manta');
INSERT INTO CANTON_UBICACION VALUES('002','Montecristi');
INSERT INTO CANTON_UBICACION VALUES('003','Jaramijo');

/* INSERTAR DATOS: UBICACION                          */
INSERT INTO UBICACION VALUES('01','001','001','Ave 1','Frente al Terminal');
INSERT INTO UBICACION VALUES('02','001','001','Calle 13','Cerca de Super PACO');
INSERT INTO UBICACION VALUES('03','002','001',NULL,'Cerro de montecristi');
INSERT INTO UBICACION VALUES('04','002','001',NULL,NULL);
INSERT INTO UBICACION VALUES('05','002','001',NULL,NULL);
INSERT INTO UBICACION VALUES('06','002','001',NULL,NULL);
INSERT INTO UBICACION VALUES('07','007','002',NULL,'Centro Montecristi');
INSERT INTO UBICACION VALUES('08','007','002',NULL,'Centro Montecristo');
INSERT INTO UBICACION VALUES('09','007','002',NULL,'Cerro de montecristi');
INSERT INTO UBICACION VALUES('10','008','003',NULL,'Puerto');

INSERT INTO UBICACION VALUES('11','001','001','Ave 1','Frente al Terminal');
INSERT INTO UBICACION VALUES('12','001','001','Calle 13','Cerca de Super PACO');
INSERT INTO UBICACION VALUES('13','001','001',NULL,'Cerro de montecristi');
INSERT INTO UBICACION VALUES('14','002','001',NULL,NULL);
INSERT INTO UBICACION VALUES('15','007','002',NULL,'Centro Montecristi');
INSERT INTO UBICACION VALUES('16','008','003',NULL,'Puerto');
INSERT INTO UBICACION VALUES('17','008','003',NULL,'Puerto');

/* INSERTAR DATOS: INCIDENTE                          */
INSERT INTO INCIDENTE VALUES(DEFAULT,'01','1','2013','1001','13021','2020-01-16 19:54:20','Poste de luz se destruyo arrancando cables electricos');
INSERT INTO INCIDENTE VALUES(DEFAULT,'02','2','2004','1002','13021','2020-02-24 15:20:24','Atropello de un hombre en la calle 13');
INSERT INTO INCIDENTE VALUES(DEFAULT,'03','1','2012','1003','13021','2020-04-08 16:55:24','Joven queda lesionado');
INSERT INTO INCIDENTE VALUES(DEFAULT,'04','2','2014','1004','13021','2020-07-16 19:54:20','Se rego quimicos peligrosos en un negocio');
INSERT INTO INCIDENTE VALUES(DEFAULT,'05','2','2006','1005','13021','2020-08-24 15:20:24','Carro se incendia luego de un choque');
INSERT INTO INCIDENTE VALUES(DEFAULT,'06','1','2004','1006','13023','2020-11-08 16:55:24','Pareja quedan lesionado despues de un accidente vehicular');
INSERT INTO INCIDENTE VALUES(DEFAULT,'07','2','2013','1007','13023','2020-11-08 16:55:24','Electricista sufre quemaduras de 2er  grado');
INSERT INTO INCIDENTE VALUES(DEFAULT,'08','3','2010','1008','13023','2020-11-08 16:55:24','Incendio en un edificio');
INSERT INTO INCIDENTE VALUES(DEFAULT,'09','1','2012','1009','13023','2020-11-08 16:55:24','Joven queda lesionado en el cerro');
INSERT INTO INCIDENTE VALUES(DEFAULT,'10','3','2010','1010','13022','2020-12-16 19:54:20','Incendio en el puerto');

INSERT INTO INCIDENTE VALUES(DEFAULT,'11','1','2013','1011','13021','2021-02-16 19:54:20','Poste de luz se destruyo arrancando cables electricos');
INSERT INTO INCIDENTE VALUES(DEFAULT,'12','2','2004','1012','13021','2021-05-24 15:20:24','Atropello de un hombre en la calle 13');
INSERT INTO INCIDENTE VALUES(DEFAULT,'13','1','2012','1013','13021','2021-05-08 16:55:24','Joven queda lesionado');
INSERT INTO INCIDENTE VALUES(DEFAULT,'14','2','2014','1014','13021','2021-06-16 19:54:20','Se rego quimicos peligrosos en un negocio');
INSERT INTO INCIDENTE VALUES(DEFAULT,'15','2','2013','1015','13023','2021-11-08 16:55:24','Electricista sufre quemaduras de 2er  grado');
INSERT INTO INCIDENTE VALUES(DEFAULT,'16','1','2012','1016','13022','2021-11-08 16:55:24','Joven queda lesionado');
INSERT INTO INCIDENTE VALUES(DEFAULT,'17','3','2010','1017','13022','2021-12-16 19:54:20','Incendio en el puerto');

/* INSERTAR DATOS: BOMBERO                         */
INSERT INTO BOMBERO VALUES('1001','13021','RUBEN','GONZALO','1315384656', '0951336448', 'AVENIDA 10 CALLE 10','2010-05-16','O+');
INSERT INTO BOMBERO VALUES('1002','13021','DARIO','VILLAMAR','1315386791', '0951336495', 'PLAZA CIVICA SAN ANDRES','2015-07-26','O+');
INSERT INTO BOMBERO VALUES('1003','13021','GONZALO','SALCEDO','1315388246', '0951336486', 'MERCADO MUNICIPAL','2020-03-20','A+');
INSERT INTO BOMBERO VALUES('1004','13021','JOSE','VARGAS','1315389634', '0951336861', 'FLAVIO REYES','2009-09-21','A+');
INSERT INTO BOMBERO VALUES('1005','13021','JUAN','REYES','1315381678', '0951336635', 'MUELLE PESQUERO ARTESANALA ','2005-11-30','O+');
INSERT INTO BOMBERO VALUES('1006','13021','DIEGO','LOPEZ','1315383558', '0951336715', 'PARQUE CENTRAL MONTECRISTI','2013-01-20','B+');
INSERT INTO BOMBERO VALUES('1007','13021','LUIS','CARREÑO','1315387511', '0951338314', 'SANTA MARTHA','2017-06-03','O+');
INSERT INTO BOMBERO VALUES('1008','13021','ERICK','MERO','1315388622', '0951337628', 'MALL DEL PACIFICO','2012-03-11','B+');
INSERT INTO BOMBERO VALUES('1009','13021','PEDRO','REY','1315212182', '0951337628', 'CENTRO DE MANTA','2012-03-11','B+');           


/* INSERTAR DATOS: CAPACITACION                      */
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Brigada de Ecuacio','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Comando en Incidentes','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Manejo de Extintores','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Materiales Peligrosos','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Rescate Basico','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Simulacion de Operacion de Vehiculo','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Brigada contra Incendios','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Brigada de primeros Auxilios','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Gestion de Riesgo','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Seguridad de Trabajo en Alturas','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Seguridad en Trabajos en espacios confinados','2020-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Brigada de Ecuacio','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Comando en Incidentes','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Manejo de Extintores','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Materiales Peligrosos','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Rescate Basico','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Brigada contra Incendios','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1001','Brigada de primeros Auxilios','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Gestion de Riesgo','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Seguridad de Trabajo en Alturas','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Seguridad en Trabajos en espacios confinados','2021-02-12','20');
INSERT INTO CAPACITACION VALUES(DEFAULT,'1002','Seguridad en Trabajos en espacios confinados','2021-02-12','20');

 
/* INSERTAR DATOS: ASCENSO                          */
INSERT INTO ASCENSO VALUES('01','1001','01','2018-10-20');
INSERT INTO ASCENSO VALUES('02','1006','02','2018-10-20');
INSERT INTO ASCENSO VALUES('03','1001','03','2018-10-20');
INSERT INTO ASCENSO VALUES('04','1005','01','2018-10-20');
INSERT INTO ASCENSO VALUES('05','1002','01','2018-10-20');
INSERT INTO ASCENSO VALUES('06','1005','02','2018-10-20');
INSERT INTO ASCENSO VALUES('07','1009','03','2018-10-20');
INSERT INTO ASCENSO VALUES('08','1003','01','2018-10-20');
INSERT INTO ASCENSO VALUES('09','1008','01','2018-10-20');
INSERT INTO ASCENSO VALUES('10','1003','02','2018-10-20');
INSERT INTO ASCENSO VALUES('11','1007','01','2019-10-20');
INSERT INTO ASCENSO VALUES('12','1008','02','2019-10-20');
INSERT INTO ASCENSO VALUES('13','1002','01','2019-10-20');
INSERT INTO ASCENSO VALUES('14','1004','02','2019-10-20');
INSERT INTO ASCENSO VALUES('15','1006','02','2019-10-20');
INSERT INTO ASCENSO VALUES('16','1001','01','2019-10-20');
INSERT INTO ASCENSO VALUES('17','1009','03','2019-10-20');
INSERT INTO ASCENSO VALUES('18','1004','01','2020-10-20');
INSERT INTO ASCENSO VALUES('19','1005','02','2020-10-20');
INSERT INTO ASCENSO VALUES('20','1001','01','2020-10-20');
INSERT INTO ASCENSO VALUES('21','1007','01','2020-10-20');
INSERT INTO ASCENSO VALUES('22','1003','02','2020-10-20');
INSERT INTO ASCENSO VALUES('23','1009','03','2020-10-20');
INSERT INTO ASCENSO VALUES('24','1001','01','2020-10-20');
INSERT INTO ASCENSO VALUES('25','1008','02','2021-10-20');
INSERT INTO ASCENSO VALUES('26','1002','01','2021-10-20');
INSERT INTO ASCENSO VALUES('27','1004','02','2021-10-20');
INSERT INTO ASCENSO VALUES('28','1006','03','2021-10-20');
INSERT INTO ASCENSO VALUES('29','1004','01','2021-10-20');
INSERT INTO ASCENSO VALUES('30','1008','02','2021-10-20');
INSERT INTO ASCENSO VALUES('31','1002','02','2021-10-20');
INSERT INTO ASCENSO VALUES('32','1001','03','2021-10-20');


/* INSERTAR DATOS: PRUEBA_NOTA                          */
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'01','18','19','19','18','17');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'02','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'03','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'04','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'05','18','19','19','18','17');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'06','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'07','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'08','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'09','18','19','19','18','17');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'10','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'11','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'12','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'13','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'14','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'15','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'16','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'17','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'18','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'19','20','18','17','20','19');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'20','20','18','19','20','18');
INSERT INTO PRUEBA_NOTA VALUES(DEFAULT,'21','20','18','19','20','18');


/* INSERTAR DATOS: VEHICULOS                          */
INSERT INTO VEHICULOS VALUES('001','13021','1007','MED395','SI');
INSERT INTO VEHICULOS VALUES('002','13021','1004','MED733','SI');
INSERT INTO VEHICULOS VALUES('003','13021','1005','MED947','SI');
INSERT INTO VEHICULOS VALUES('004','13021','1002','MED248','SI');

/* INSERTAR DATOS: MANTENIMIENTO_VEHICULO                          */
INSERT INTO MANTENIMIENTO_VEHICULO VALUES('001','004','2021-03-20','150');
INSERT INTO MANTENIMIENTO_VEHICULO VALUES('002','002','2021-07-11','260');
INSERT INTO MANTENIMIENTO_VEHICULO VALUES('003','001','2021-11-30','180');

/* INSERTAR DATOS: ORIGEN_EQUIPOS                        */
INSERT INTO ORIGEN_EQUIPOS VALUES('001','Gobierno Municipal');
INSERT INTO ORIGEN_EQUIPOS VALUES('002','Gobierno Nacional');
INSERT INTO ORIGEN_EQUIPOS VALUES('003','IFSA');  /*Asociación Internacional de Seguridad contra Incendios*/
INSERT INTO ORIGEN_EQUIPOS VALUES('004','OPS');  /* Organización Panamericana de la Salud*/

/* INSERTAR DATOS: EQUIPO                        */
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Casco','100');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Visor','80');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Protector de cuello','80');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Chaqueta','100');   
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Botas','100');                 
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Extintores','30');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Sierra','30');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Herramienta de zapa','50');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Equipo de Respiracion Autonoma','50');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Manguera','30');            
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Escalera','10');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Equipo de proteccion personal','75');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Cuerdas elasticas','20');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Camilla','55');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Expansor','30');             
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Bomba de Succion','25');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Equipo de Buceo','40');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Martillo de Impacto','45');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Camaras de busqueda','70');
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Generador Electrico','25');  
INSERT INTO EQUIPOS VALUES(DEFAULT,'13021','Mascarilla','500');
 

/* INSERTAR DATOS: INGRESO_EQUIPOS                        
INSERT INTO INGRESO_EQUIPOS VALUES('001','002','007','50','2020-01-23');
INSERT INTO INGRESO_EQUIPOS VALUES('002','003','004','200','2020-06-18');
INSERT INTO INGRESO_EQUIPOS VALUES('003','001','003','200','2020-11-30');
INSERT INTO INGRESO_EQUIPOS VALUES('004','002','014','3000','2021-02-25');
INSERT INTO INGRESO_EQUIPOS VALUES('005','004','005','200','2021-07-03'); */

/* INSERTAR DATOS: ASIGNACION                        
INSERT INTO ASIGNACION VALUES('01','001','1001','1','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('02','002','1001','4','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('03','003','1001','2','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('04','004','1001','1','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('05','005','1001','2','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('06','006','1001','1','2020-01-15','ASIGNADO');

INSERT INTO ASIGNACION VALUES('07','001','1002','1','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('08','002','1002','4','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('09','003','1002','2','2020-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('10','004','1002','1','2020-01-15','ASIGNADO');

INSERT INTO ASIGNACION VALUES('11','007','1001','1','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('12','008','1001','1','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('13','021','1001','21','2021-01-15','ASIGNADO');

INSERT INTO ASIGNACION VALUES('14','005','1002','2','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('15','006','1002','1','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('16','007','1002','1','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('17','008','1002','1','2021-01-15','ASIGNADO');
INSERT INTO ASIGNACION VALUES('18','021','1002','25','2021-01-15','ASIGNADO');  */


/* INSERTAR DATOS: DEVOLUCION                        
INSERT INTO DEVOLUCION VALUES(DEFAULT,'017','2020-12-25','1');
INSERT INTO DEVOLUCION VALUES(DEFAULT,'013','2020-12-25','21');
INSERT INTO DEVOLUCION VALUES(DEFAULT,'001','2020-12-25','1');
INSERT INTO DEVOLUCION VALUES(DEFAULT,'007','2020-12-25','1');      */

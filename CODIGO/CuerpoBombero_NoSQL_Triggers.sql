/*
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
---------------------------------------TRIGGERS - PosgreSQL
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

CREATE FUNCTION ASIGNAR_EQUIPOS() RETURNS TRIGGER AS $ASIGNAR_EQUIPOS$
DECLARE
	CANTIDAD INTEGER;
BEGIN
	SELECT (ASIGNACION_CANTIDAD) INTO CANTIDAD FROM ASIGNACION 
	--Si el codigo del ide es igual al codigo ingresado y si el id del bombero es igual al id ingresado
	WHERE EQUIPOS_ID=NEW.EQUIPOS_ID AND BOMBERO_ID=NEW.BOMBERO_ID;
	IF (CANTIDAD > 0) THEN
		RAISE EXCEPTION 'EL BOMBERO YA TIENE ASIGNADO UN EQUIPO';
	END IF;
	RETURN NEW;	 
END;
$ASIGNAR_EQUIPOS$ LANGUAGE plpgsql;



--Creacion del Trigger
CREATE TRIGGER PR_ASIGNACION 
BEFORE INSERT ON ASIGNACION FOR EACH ROW
EXECUTE PROCEDURE ASIGNAR_EQUIPOS();


--INSERT INTO ASIGNACION VALUES('19','021','1002','15','2021-11-15');


/*
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
---------------------------------------TRIGGER - JAVASCRIPT
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


exports.onUserCreate = functions.firestore
    .document("Asignacion/{userId}")
    .onCreate(async (snap, context) => {
        const datos = snap.data();
        //Se realiza una consulta de los equipos asignados, cuando la Id de equipos y equipos sean iguales
        let query = db.collection("Asignacion").where("Bombero_id", "==", datos.Bombero_id).where("Equipos_id", "==", datos.Equipos_id);
        await query.get().then((snap) => {
            size = snap.size;
        });

        console.log(size);
        if (size > 1 ){            
            db.collection("Asignacion").doc(snap.id).delete()
            console.log("El bombero ya tiene asignado ese equipo");
        } else {
            console.log("El equipo se asigno con exito");
        }
    });
 
 
 
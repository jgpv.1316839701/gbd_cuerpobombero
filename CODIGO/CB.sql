/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     18/1/2022 18:39:53                           */
/*==============================================================*/


drop index RELATIONSHIP_17_FK;

drop index RELATIONSHIP_16_FK;

drop index ASCENSO_PK;

drop table ASCENSO;

drop index RELATIONSHIP_23_FK;

drop index RELATIONSHIP_22_FK;

drop index ASIGNACION_PK;

drop table ASIGNACION;

drop index RELATIONSHIP_9_FK;

drop index BOMBERO_PK;

drop table BOMBERO;

drop index CANTON_UBICACION_PK;

drop table CANTON_UBICACION;

drop index RELATIONSHIP_27_FK;

drop index CAPACITACION_PK;

drop table CAPACITACION;

drop index RELATIONSHIP_28_FK;

drop index DEVOLUCION_PK;

drop table DEVOLUCION;

drop index DISTRITO_PK;

drop table DISTRITO;

drop index RELATIONSHIP_21_FK;

drop index EQUIPOS_PK;

drop table EQUIPOS;

drop index RELATIONSHIP_12_FK;

drop index RELATIONSHIP_8_FK;

drop index RELATIONSHIP_7_FK;

drop index RELATIONSHIP_6_FK;

drop index RELATIONSHIP_5_FK;

drop index INCIDENTE_PK;

drop table INCIDENTE;

drop index RELATIONSHIP_26_FK;

drop index RELATIONSHIP_20_FK;

drop index INGRESO_EQUIPOS_PK;

drop table INGRESO_EQUIPOS;

drop index RELATIONSHIP_15_FK;

drop index MANTENIMIENTO_VEHICULO_PK;

drop table MANTENIMIENTO_VEHICULO;

drop index NIVEL_PELIGRO_PK;

drop table NIVEL_PELIGRO;

drop index ORIGEN_EQUIPOS_PK;

drop table ORIGEN_EQUIPOS;

drop index RELATIONSHIP_18_FK;

drop index PRUEBA_NOTA_PK;

drop table PRUEBA_NOTA;

drop index RANGO_PK;

drop table RANGO;

drop index REPORTADOR_PK;

drop table REPORTADOR;

drop index SECTOR_UBICACION_PK;

drop table SECTOR_UBICACION;

drop index TIPO_INDICENTE_PK;

drop table TIPO_INDICENTE;

drop index RELATIONSHIP_4_FK;

drop index RELATIONSHIP_3_FK;

drop index UBICACION_PK;

drop table UBICACION;

drop index RELATIONSHIP_24_FK;

drop index RELATIONSHIP_25_FK;

drop index VEHICULOS_PK;

drop table VEHICULOS;

/*==============================================================*/
/* Table: ASCENSO                                               */
/*==============================================================*/
create table ASCENSO (
   ASCENSO_ID           SERIAL               not null,
   BOMBERO_ID           INT4                 not null,
   RANGO_ID             INT4                 not null,
   ASCENSO_FECHA        DATE                 not null,
   constraint PK_ASCENSO primary key (ASCENSO_ID)
);

/*==============================================================*/
/* Index: ASCENSO_PK                                            */
/*==============================================================*/
create unique index ASCENSO_PK on ASCENSO (
ASCENSO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_16_FK on ASCENSO (
BOMBERO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_17_FK on ASCENSO (
RANGO_ID
);

/*==============================================================*/
/* Table: ASIGNACION                                            */
/*==============================================================*/
create table ASIGNACION (
   ASIGNACION_ID        SERIAL               not null,
   EQUIPOS_ID           INT4                 not null,
   BOMBERO_ID           INT4                 not null,
   ASIGNACION_CANTIDAD  NUMERIC              not null,
   ASIGNACION_FECHA     DATE                 not null,
   constraint PK_ASIGNACION primary key (ASIGNACION_ID)
);

/*==============================================================*/
/* Index: ASIGNACION_PK                                         */
/*==============================================================*/
create unique index ASIGNACION_PK on ASIGNACION (
ASIGNACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_22_FK on ASIGNACION (
EQUIPOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_23_FK on ASIGNACION (
BOMBERO_ID
);

/*==============================================================*/
/* Table: BOMBERO                                               */
/*==============================================================*/
create table BOMBERO (
   BOMBERO_ID           SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   BOMBERO_NOMBRE       TEXT                 not null,
   BOMBERO_APELLIDO     TEXT                 not null,
   BOMBERO_CEDULA       NUMERIC(10)          not null,
   BOMBERO_TELEFONO     NUMERIC(10)          not null,
   BOMBERO_DIRECCION    TEXT                 not null,
   BOMBERO_FECHA_CONTRATO DATE                 not null,
   BOMBERO_TIPO_SANGRE  TEXT                 not null,
   constraint PK_BOMBERO primary key (BOMBERO_ID)
);

/*==============================================================*/
/* Index: BOMBERO_PK                                            */
/*==============================================================*/
create unique index BOMBERO_PK on BOMBERO (
BOMBERO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on BOMBERO (
DISTRITO_ID
);

/*==============================================================*/
/* Table: CANTON_UBICACION                                      */
/*==============================================================*/
create table CANTON_UBICACION (
   CANTON_ID            SERIAL               not null,
   CANTON_NOMBRE        TEXT                 null,
   constraint PK_CANTON_UBICACION primary key (CANTON_ID)
);

/*==============================================================*/
/* Index: CANTON_UBICACION_PK                                   */
/*==============================================================*/
create unique index CANTON_UBICACION_PK on CANTON_UBICACION (
CANTON_ID
);

/*==============================================================*/
/* Table: CAPACITACION                                          */
/*==============================================================*/
create table CAPACITACION (
   CAPACITACION_ID      SERIAL               not null,
   BOMBERO_ID           INT4                 not null,
   CAPACITACION_NOMBRE  TEXT                 not null,
   CAPACITACION_FECHA   DATE                 not null,
   CAPACITACION_NOTA    NUMERIC(2)           not null,
   constraint PK_CAPACITACION primary key (CAPACITACION_ID)
);

/*==============================================================*/
/* Index: CAPACITACION_PK                                       */
/*==============================================================*/
create unique index CAPACITACION_PK on CAPACITACION (
CAPACITACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_27_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_27_FK on CAPACITACION (
BOMBERO_ID
);

/*==============================================================*/
/* Table: DEVOLUCION                                            */
/*==============================================================*/
create table DEVOLUCION (
   DEVOLUCION_ID        SERIAL               not null,
   ASIGNACION_ID        INT4                 not null,
   DEVOLUCION_FECHA     DATE                 not null,
   DEVOLUCION_CANTIDAD  NUMERIC              not null,
   constraint PK_DEVOLUCION primary key (DEVOLUCION_ID)
);

/*==============================================================*/
/* Index: DEVOLUCION_PK                                         */
/*==============================================================*/
create unique index DEVOLUCION_PK on DEVOLUCION (
DEVOLUCION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_28_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_28_FK on DEVOLUCION (
ASIGNACION_ID
);

/*==============================================================*/
/* Table: DISTRITO                                              */
/*==============================================================*/
create table DISTRITO (
   DISTRITO_ID          SERIAL               not null,
   DISTRITO_CANTON      TEXT                 null,
   DISTRITO_SECTOR      TEXT                 null,
   constraint PK_DISTRITO primary key (DISTRITO_ID)
);

/*==============================================================*/
/* Index: DISTRITO_PK                                           */
/*==============================================================*/
create unique index DISTRITO_PK on DISTRITO (
DISTRITO_ID
);

/*==============================================================*/
/* Table: EQUIPOS                                               */
/*==============================================================*/
create table EQUIPOS (
   EQUIPOS_ID           SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   EQUIPOS_NOMBRE       TEXT                 null,
   EQUIPOS_CANTIDAD     NUMERIC              null,
   constraint PK_EQUIPOS primary key (EQUIPOS_ID)
);

/*==============================================================*/
/* Index: EQUIPOS_PK                                            */
/*==============================================================*/
create unique index EQUIPOS_PK on EQUIPOS (
EQUIPOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_21_FK on EQUIPOS (
DISTRITO_ID
);

/*==============================================================*/
/* Table: INCIDENTE                                             */
/*==============================================================*/
create table INCIDENTE (
   INCIDENTE_ID         SERIAL               not null,
   UBICACION_ID         INT4                 not null,
   NIVEL_PELIGRO_ID     INT4                 not null,
   TIPO_INDICENTE_ID    INT4                 not null,
   REPORTADOR_ID        INT4                 not null,
   DISTRITO_ID          INT4                 not null,
   INCIDENTE_FECHA      DATE                 null,
   INCIDENTE_DESCRIPCION TEXT                 null,
   constraint PK_INCIDENTE primary key (INCIDENTE_ID)
);

/*==============================================================*/
/* Index: INCIDENTE_PK                                          */
/*==============================================================*/
create unique index INCIDENTE_PK on INCIDENTE (
INCIDENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on INCIDENTE (
UBICACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on INCIDENTE (
NIVEL_PELIGRO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on INCIDENTE (
TIPO_INDICENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on INCIDENTE (
REPORTADOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on INCIDENTE (
DISTRITO_ID
);

/*==============================================================*/
/* Table: INGRESO_EQUIPOS                                       */
/*==============================================================*/
create table INGRESO_EQUIPOS (
   INGRESO_ID           SERIAL               not null,
   ORIGEN_ID            INT4                 not null,
   EQUIPOS_ID           INT4                 not null,
   INGRESO_CANTIDAD     NUMERIC              null,
   INGRESO_FECHA        DATE                 null,
   constraint PK_INGRESO_EQUIPOS primary key (INGRESO_ID)
);

/*==============================================================*/
/* Index: INGRESO_EQUIPOS_PK                                    */
/*==============================================================*/
create unique index INGRESO_EQUIPOS_PK on INGRESO_EQUIPOS (
INGRESO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_20_FK on INGRESO_EQUIPOS (
ORIGEN_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_26_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_26_FK on INGRESO_EQUIPOS (
EQUIPOS_ID
);

/*==============================================================*/
/* Table: MANTENIMIENTO_VEHICULO                                */
/*==============================================================*/
create table MANTENIMIENTO_VEHICULO (
   MANTENIMIENTO_ID     SERIAL               not null,
   VEHICULOS_ID         INT4                 not null,
   MANTENIMIENTO_FECHA  DATE                 null,
   MANTENIMIENTO_GASTO  MONEY                null,
   constraint PK_MANTENIMIENTO_VEHICULO primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Index: MANTENIMIENTO_VEHICULO_PK                             */
/*==============================================================*/
create unique index MANTENIMIENTO_VEHICULO_PK on MANTENIMIENTO_VEHICULO (
MANTENIMIENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_15_FK on MANTENIMIENTO_VEHICULO (
VEHICULOS_ID
);

/*==============================================================*/
/* Table: NIVEL_PELIGRO                                         */
/*==============================================================*/
create table NIVEL_PELIGRO (
   NIVEL_PELIGRO_ID     SERIAL               not null,
   NIVEL_PELIGRO_NOMBRE TEXT                 null,
   constraint PK_NIVEL_PELIGRO primary key (NIVEL_PELIGRO_ID)
);

/*==============================================================*/
/* Index: NIVEL_PELIGRO_PK                                      */
/*==============================================================*/
create unique index NIVEL_PELIGRO_PK on NIVEL_PELIGRO (
NIVEL_PELIGRO_ID
);

/*==============================================================*/
/* Table: ORIGEN_EQUIPOS                                        */
/*==============================================================*/
create table ORIGEN_EQUIPOS (
   ORIGEN_ID            SERIAL               not null,
   ORIGEN_NOMBRE        TEXT                 null,
   constraint PK_ORIGEN_EQUIPOS primary key (ORIGEN_ID)
);

/*==============================================================*/
/* Index: ORIGEN_EQUIPOS_PK                                     */
/*==============================================================*/
create unique index ORIGEN_EQUIPOS_PK on ORIGEN_EQUIPOS (
ORIGEN_ID
);

/*==============================================================*/
/* Table: PRUEBA_NOTA                                           */
/*==============================================================*/
create table PRUEBA_NOTA (
   PRUEBA_ID            SERIAL               not null,
   ASCENSO_ID           INT4                 not null,
   PRUEBA_ACADEMICA     NUMERIC(2)           not null,
   PRUEBA_CONFIANZA     NUMERIC(2)           not null,
   PRUEBA_PSICOMETRICAS NUMERIC(2)           not null,
   PRUEBA_FISICAS       NUMERIC(2)           not null,
   PRUEBA_ENTREVISTA    NUMERIC(2)           not null,
   constraint PK_PRUEBA_NOTA primary key (PRUEBA_ID)
);

/*==============================================================*/
/* Index: PRUEBA_NOTA_PK                                        */
/*==============================================================*/
create unique index PRUEBA_NOTA_PK on PRUEBA_NOTA (
PRUEBA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_18_FK on PRUEBA_NOTA (
ASCENSO_ID
);

/*==============================================================*/
/* Table: RANGO                                                 */
/*==============================================================*/
create table RANGO (
   RANGO_ID             SERIAL               not null,
   RANGO_NOMBRE         TEXT                 null,
   RANGO_ANOS_NECESARIOS NUMERIC(2)           null,
   constraint PK_RANGO primary key (RANGO_ID)
);

/*==============================================================*/
/* Index: RANGO_PK                                              */
/*==============================================================*/
create unique index RANGO_PK on RANGO (
RANGO_ID
);

/*==============================================================*/
/* Table: REPORTADOR                                            */
/*==============================================================*/
create table REPORTADOR (
   REPORTADOR_ID        SERIAL               not null,
   REPORTADOR_NOMBRE    TEXT                 null,
   REPORTADOR_APELLIDO  TEXT                 null,
   REPORTADOR_FECHA     DATE                 null,
   constraint PK_REPORTADOR primary key (REPORTADOR_ID)
);

/*==============================================================*/
/* Index: REPORTADOR_PK                                         */
/*==============================================================*/
create unique index REPORTADOR_PK on REPORTADOR (
REPORTADOR_ID
);

/*==============================================================*/
/* Table: SECTOR_UBICACION                                      */
/*==============================================================*/
create table SECTOR_UBICACION (
   SECTOR_ID            SERIAL               not null,
   SECTOR_NOMBRE        TEXT                 null,
   constraint PK_SECTOR_UBICACION primary key (SECTOR_ID)
);

/*==============================================================*/
/* Index: SECTOR_UBICACION_PK                                   */
/*==============================================================*/
create unique index SECTOR_UBICACION_PK on SECTOR_UBICACION (
SECTOR_ID
);

/*==============================================================*/
/* Table: TIPO_INDICENTE                                        */
/*==============================================================*/
create table TIPO_INDICENTE (
   TIPO_INDICENTE_ID    SERIAL               not null,
   TIPO_INDICENTE_NOMBRE TEXT                 null,
   constraint PK_TIPO_INDICENTE primary key (TIPO_INDICENTE_ID)
);

/*==============================================================*/
/* Index: TIPO_INDICENTE_PK                                     */
/*==============================================================*/
create unique index TIPO_INDICENTE_PK on TIPO_INDICENTE (
TIPO_INDICENTE_ID
);

/*==============================================================*/
/* Table: UBICACION                                             */
/*==============================================================*/
create table UBICACION (
   UBICACION_ID         SERIAL               not null,
   SECTOR_ID            INT4                 not null,
   CANTON_ID            INT4                 not null,
   UBICACION_DIRECCION  TEXT                 null,
   UBICACION_REFERENCIA TEXT                 null,
   constraint PK_UBICACION primary key (UBICACION_ID)
);

/*==============================================================*/
/* Index: UBICACION_PK                                          */
/*==============================================================*/
create unique index UBICACION_PK on UBICACION (
UBICACION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on UBICACION (
SECTOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on UBICACION (
CANTON_ID
);

/*==============================================================*/
/* Table: VEHICULOS                                             */
/*==============================================================*/
create table VEHICULOS (
   VEHICULOS_ID         SERIAL               not null,
   DISTRITO_ID          INT4                 not null,
   BOMBERO_ID           INT4                 not null,
   VEHICULOS_PLACA      TEXT                 null,
   VEHICULOS_OPERATIVO  TEXT                 null,
   constraint PK_VEHICULOS primary key (VEHICULOS_ID)
);

/*==============================================================*/
/* Index: VEHICULOS_PK                                          */
/*==============================================================*/
create unique index VEHICULOS_PK on VEHICULOS (
VEHICULOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_25_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_25_FK on VEHICULOS (
DISTRITO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_24_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_24_FK on VEHICULOS (
BOMBERO_ID
);

alter table ASCENSO
   add constraint FK_ASCENSO_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table ASCENSO
   add constraint FK_ASCENSO_RELATIONS_RANGO foreign key (RANGO_ID)
      references RANGO (RANGO_ID)
      on delete restrict on update restrict;

alter table ASIGNACION
   add constraint FK_ASIGNACI_RELATIONS_EQUIPOS foreign key (EQUIPOS_ID)
      references EQUIPOS (EQUIPOS_ID)
      on delete restrict on update restrict;

alter table ASIGNACION
   add constraint FK_ASIGNACI_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table BOMBERO
   add constraint FK_BOMBERO_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table CAPACITACION
   add constraint FK_CAPACITA_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table DEVOLUCION
   add constraint FK_DEVOLUCI_RELATIONS_ASIGNACI foreign key (ASIGNACION_ID)
      references ASIGNACION (ASIGNACION_ID)
      on delete restrict on update restrict;

alter table EQUIPOS
   add constraint FK_EQUIPOS_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_UBICACIO foreign key (UBICACION_ID)
      references UBICACION (UBICACION_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_NIVEL_PE foreign key (NIVEL_PELIGRO_ID)
      references NIVEL_PELIGRO (NIVEL_PELIGRO_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_TIPO_IND foreign key (TIPO_INDICENTE_ID)
      references TIPO_INDICENTE (TIPO_INDICENTE_ID)
      on delete restrict on update restrict;

alter table INCIDENTE
   add constraint FK_INCIDENT_RELATIONS_REPORTAD foreign key (REPORTADOR_ID)
      references REPORTADOR (REPORTADOR_ID)
      on delete restrict on update restrict;

alter table INGRESO_EQUIPOS
   add constraint FK_INGRESO__RELATIONS_ORIGEN_E foreign key (ORIGEN_ID)
      references ORIGEN_EQUIPOS (ORIGEN_ID)
      on delete restrict on update restrict;

alter table INGRESO_EQUIPOS
   add constraint FK_INGRESO__RELATIONS_EQUIPOS foreign key (EQUIPOS_ID)
      references EQUIPOS (EQUIPOS_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO_VEHICULO
   add constraint FK_MANTENIM_RELATIONS_VEHICULO foreign key (VEHICULOS_ID)
      references VEHICULOS (VEHICULOS_ID)
      on delete restrict on update restrict;

alter table PRUEBA_NOTA
   add constraint FK_PRUEBA_N_RELATIONS_ASCENSO foreign key (ASCENSO_ID)
      references ASCENSO (ASCENSO_ID)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_RELATIONS_SECTOR_U foreign key (SECTOR_ID)
      references SECTOR_UBICACION (SECTOR_ID)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_RELATIONS_CANTON_U foreign key (CANTON_ID)
      references CANTON_UBICACION (CANTON_ID)
      on delete restrict on update restrict;

alter table VEHICULOS
   add constraint FK_VEHICULO_RELATIONS_BOMBERO foreign key (BOMBERO_ID)
      references BOMBERO (BOMBERO_ID)
      on delete restrict on update restrict;

alter table VEHICULOS
   add constraint FK_VEHICULO_RELATIONS_DISTRITO foreign key (DISTRITO_ID)
      references DISTRITO (DISTRITO_ID)
      on delete restrict on update restrict;



package CuerpoBombero;
import sql.crudsql;
import Mostrar.capacitacion_variables;

public class Capacitacion extends javax.swing.JFrame {

    
    public Capacitacion() {
        initComponents();
        
        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(false);
        c_capacitacion.setEditable(false);
        c_fecha.setEditable(false);
        c_nota.setEditable(false);
        //FOCUS
        c_codigo.requestFocus();
        //BLANCO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_capacitacion.setEnabled(true);
        guardar_capacitacion.setEnabled(false);; 
        actualizar_capacitacion.setEnabled(false);
        eliminar_capacitacion.setEnabled(false);
    } 
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buscar_capacitacion = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        c_codigo = new javax.swing.JTextField();
        Boton_Menu = new javax.swing.JButton();
        nuevo_capacitacion = new javax.swing.JButton();
        reinicio = new javax.swing.JButton();
        c_fecha = new javax.swing.JTextField();
        c_nota = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        actualizar_capacitacion = new javax.swing.JButton();
        guardar_capacitacion = new javax.swing.JButton();
        eliminar_capacitacion = new javax.swing.JButton();
        c_bombero = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        c_capacitacion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buscar_capacitacion.setText("BUSCAR");
        buscar_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar_capacitacionActionPerformed(evt);
            }
        });

        jLabel10.setText("Buscar Codigo");

        c_codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c_codigoActionPerformed(evt);
            }
        });

        Boton_Menu.setText("Menu");
        Boton_Menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Boton_MenuActionPerformed(evt);
            }
        });

        nuevo_capacitacion.setText("NUEVO");
        nuevo_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevo_capacitacionActionPerformed(evt);
            }
        });

        reinicio.setText("Volver");
        reinicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reinicioActionPerformed(evt);
            }
        });

        c_fecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c_fechaActionPerformed(evt);
            }
        });

        c_nota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c_notaActionPerformed(evt);
            }
        });

        jLabel6.setText("Fecha:");

        jLabel7.setText("Nota:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("CAPACITACION");

        actualizar_capacitacion.setText("ACTUALIZAR");
        actualizar_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actualizar_capacitacionActionPerformed(evt);
            }
        });

        guardar_capacitacion.setText("GUARDAR");
        guardar_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardar_capacitacionActionPerformed(evt);
            }
        });

        eliminar_capacitacion.setText("ELIMINAR");
        eliminar_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar_capacitacionActionPerformed(evt);
            }
        });

        c_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c_bomberoActionPerformed(evt);
            }
        });

        jLabel1.setText("Id Bombero");

        c_capacitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c_capacitacionActionPerformed(evt);
            }
        });

        jLabel2.setText("Capacitacion:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Boton_Menu)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(reinicio)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(c_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(c_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(nuevo_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(c_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(guardar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(c_nota, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel10))
                    .addComponent(eliminar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buscar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actualizar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(c_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Boton_Menu)
                        .addComponent(reinicio))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(c_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buscar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(actualizar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(c_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(c_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(c_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(c_nota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(eliminar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(guardar_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevo_capacitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buscar_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar_capacitacionActionPerformed
        crudsql crud_c = new crudsql();
        capacitacion_variables cap = new capacitacion_variables();
        crud_c.mostrar_capacitacion(c_codigo.getText());
        c_bombero.setText(cap.getBombero_id());
        c_capacitacion.setText(cap.getCapacitacion_nombre());
        c_fecha.setText(cap.getCapacitacion_fecha());
        c_nota.setText(cap.getCapacitacion_nota());

        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(true);
        c_capacitacion.setEditable(true);
        c_fecha.setEditable(true);
        c_nota.setEditable(true);
        //BOTON
        reinicio.setVisible(true);
        actualizar_capacitacion.setEnabled(true);
        eliminar_capacitacion.setEnabled(true);
        guardar_capacitacion.setEnabled(false);

    }//GEN-LAST:event_buscar_capacitacionActionPerformed

    private void c_codigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c_codigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c_codigoActionPerformed

    private void Boton_MenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Boton_MenuActionPerformed
        Menu objmenu = new Menu();
        objmenu.setVisible(true);
        objmenu.setLocationRelativeTo(null);
        this.setVisible(false);
    }//GEN-LAST:event_Boton_MenuActionPerformed

    private void nuevo_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevo_capacitacionActionPerformed

        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(false);
        c_bombero.setEditable(true);
        c_capacitacion.setEditable(true);
        c_fecha.setEditable(true);
        c_nota.setEditable(true);
        //FOCUS
        c_bombero.requestFocus();
        //NUEVO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");
        //BOTON
        reinicio.setVisible(true);
        guardar_capacitacion.setEnabled(true);
        buscar_capacitacion.setEnabled(false);
        actualizar_capacitacion.setEnabled(false);
        eliminar_capacitacion.setEnabled(false);

    }//GEN-LAST:event_nuevo_capacitacionActionPerformed

    private void reinicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reinicioActionPerformed
        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(false);
        c_capacitacion.setEditable(false);
        c_fecha.setEditable(false);
        c_nota.setEditable(false);
        //FOCUS
        c_codigo.requestFocus();
        //BLANCO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_capacitacion.setEnabled(true);
        guardar_capacitacion.setEnabled(false);
        actualizar_capacitacion.setEnabled(false);
        eliminar_capacitacion.setEnabled(false);

    }//GEN-LAST:event_reinicioActionPerformed

    private void c_fechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c_fechaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c_fechaActionPerformed

    private void c_notaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c_notaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c_notaActionPerformed

    private void actualizar_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actualizar_capacitacionActionPerformed
        crudsql crud_c = new crudsql();
        capacitacion_variables objvar = new capacitacion_variables();
        objvar.setCapacitacion_id(c_codigo.getText());
        objvar.setBombero_id(c_bombero.getText());
        objvar.setCapacitacion_nombre(c_capacitacion.getText());
        objvar.setCapacitacion_fecha(c_fecha.getText());
        objvar.setCapacitacion_nota(c_nota.getText());
        crud_c.actualizar_capacitacion(objvar.getCapacitacion_id(), objvar.getBombero_id(), objvar.getCapacitacion_nombre(),objvar.getCapacitacion_fecha(), objvar.getCapacitacion_nota());

        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(false);
        c_capacitacion.setEditable(false);
        c_fecha.setEditable(false);
        c_nota.setEditable(false);
        //FOCUS
        c_codigo.requestFocus();
        //BOTONES
        reinicio.setVisible(false);
        guardar_capacitacion.setEnabled(false);
        actualizar_capacitacion.setEnabled(false);
        eliminar_capacitacion.setEnabled(false);
        //BLANCO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");

    }//GEN-LAST:event_actualizar_capacitacionActionPerformed

    private void guardar_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardar_capacitacionActionPerformed
        crudsql crud_c = new crudsql();
        crud_c.insertar_capacitacion(c_bombero.getText(), c_capacitacion.getText(), c_fecha.getText(), c_nota.getText());

        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(false);
        c_capacitacion.setEditable(false);
        c_fecha.setEditable(false);
        c_nota.setEditable(false);
        //FOCUS
        c_codigo.requestFocus();
        //BLANCO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_capacitacion.setEnabled(true);

    }//GEN-LAST:event_guardar_capacitacionActionPerformed

    private void eliminar_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar_capacitacionActionPerformed
        crudsql crud_b = new crudsql();
        capacitacion_variables objvar = new capacitacion_variables();
        objvar.setCapacitacion_id(c_codigo.getText());
        crud_b.eliminar_capacitacion(objvar.getCapacitacion_id());

        //Bloquear Acciones en TextFiel
        c_codigo.setEditable(true);
        c_bombero.setEditable(false);
        c_capacitacion.setEditable(false);
        c_fecha.setEditable(false);
        c_nota.setEditable(false);
        //FOCUS
        c_codigo.requestFocus();
        //BLANCO
        c_codigo.setText("");
        c_bombero.setText("");
        c_capacitacion.setText("");
        c_fecha.setText("");
        c_nota.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_capacitacion.setEnabled(true);
        guardar_capacitacion.setEnabled(false);
    }//GEN-LAST:event_eliminar_capacitacionActionPerformed

    private void c_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c_bomberoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c_bomberoActionPerformed

    private void c_capacitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c_capacitacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c_capacitacionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Capacitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Capacitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Capacitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Capacitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Capacitacion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Boton_Menu;
    private javax.swing.JButton actualizar_capacitacion;
    private javax.swing.JButton buscar_capacitacion;
    private javax.swing.JTextField c_bombero;
    private javax.swing.JTextField c_capacitacion;
    private javax.swing.JTextField c_codigo;
    private javax.swing.JTextField c_fecha;
    private javax.swing.JTextField c_nota;
    private javax.swing.JButton eliminar_capacitacion;
    private javax.swing.JButton guardar_capacitacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JButton nuevo_capacitacion;
    private javax.swing.JButton reinicio;
    // End of variables declaration//GEN-END:variables
}

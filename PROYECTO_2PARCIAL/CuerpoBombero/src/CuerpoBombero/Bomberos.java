package CuerpoBombero;
import sql.crudsql;
import Mostrar.bombero_variables;

public class Bomberos extends javax.swing.JFrame {
    
    public Bomberos() {
        initComponents();
        
        //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(false);
        b_apellido.setEditable(false);
        b_cedula.setEditable(false);
        b_telefono.setEditable(false);
        b_direccion.setEditable(false);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //FOCUS
        b_codigo.requestFocus();
        //BOTONES
        reinicio.setVisible(false);
        guardar_bombero.setEnabled(false); 
        actualizar_bombero.setEnabled(false); 
        eliminar_bombero.setEnabled(false); 
                
        //TABLA DE LOS REGISTROS
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField3 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        b_nombre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        b_apellido = new javax.swing.JTextField();
        b_cedula = new javax.swing.JTextField();
        b_telefono = new javax.swing.JTextField();
        b_direccion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        b_fecha = new javax.swing.JTextField();
        b_sangre = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        actualizar_bombero = new javax.swing.JButton();
        guardar_bombero = new javax.swing.JButton();
        eliminar_bombero = new javax.swing.JButton();
        b_distrito = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        buscar_bombero = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        b_codigo = new javax.swing.JTextField();
        Boton_Menu = new javax.swing.JButton();
        nuevo_bombero = new javax.swing.JButton();
        reinicio = new javax.swing.JButton();

        jTextField3.setText("jTextField1");
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jTextField5.setText("jTextField1");
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        b_nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_nombreActionPerformed(evt);
            }
        });

        jLabel1.setText("Nombres:");

        b_apellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_apellidoActionPerformed(evt);
            }
        });

        b_cedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cedulaActionPerformed(evt);
            }
        });

        b_telefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_telefonoActionPerformed(evt);
            }
        });

        b_direccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_direccionActionPerformed(evt);
            }
        });

        jLabel2.setText("Apellidos:");

        jLabel3.setText("Cedula:");

        jLabel4.setText("Telefono:");

        b_fecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_fechaActionPerformed(evt);
            }
        });

        b_sangre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_sangreActionPerformed(evt);
            }
        });

        jLabel5.setText("Direccion:");

        jLabel6.setText("Fecha Contrato:");

        jLabel7.setText("T. Sangre:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("BOMBERO");

        actualizar_bombero.setText("ACTUALIZAR");
        actualizar_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actualizar_bomberoActionPerformed(evt);
            }
        });

        guardar_bombero.setText("GUARDAR");
        guardar_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardar_bomberoActionPerformed(evt);
            }
        });

        eliminar_bombero.setText("ELIMINAR");
        eliminar_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar_bomberoActionPerformed(evt);
            }
        });

        b_distrito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_distritoActionPerformed(evt);
            }
        });

        jLabel9.setText("Distrito");

        buscar_bombero.setText("BUSCAR");
        buscar_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar_bomberoActionPerformed(evt);
            }
        });

        jLabel10.setText("Buscar Codigo");

        b_codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_codigoActionPerformed(evt);
            }
        });

        Boton_Menu.setText("Menu");
        Boton_Menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Boton_MenuActionPerformed(evt);
            }
        });

        nuevo_bombero.setText("NUEVO");
        nuevo_bombero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevo_bomberoActionPerformed(evt);
            }
        });

        reinicio.setText("Volver");
        reinicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reinicioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Boton_Menu)
                        .addGap(98, 98, 98)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(38, 38, 38)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3))
                                        .addGap(7, 7, 7))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(b_cedula, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(22, 22, 22)
                                                    .addComponent(jLabel4)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(b_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(b_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(b_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(jLabel7)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(b_sangre, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(b_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(nuevo_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(guardar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(56, 56, 56)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(b_distrito, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(278, 278, 278)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(eliminar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(buscar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(actualizar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(b_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(reinicio)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(b_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel10)
                                        .addGap(24, 24, 24)))))))
                .addGap(27, 27, 27))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(Boton_Menu)
                                .addComponent(reinicio))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(b_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel1))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(b_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel2))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(b_cedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4)
                                            .addComponent(b_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(b_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(b_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(b_sangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel7)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(b_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(buscar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(actualizar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(b_distrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(eliminar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardar_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nuevo_bombero, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //Nos permite tener una relacion para usar los metodos sql de crudsql 
    
   
    
    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code he:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void b_apellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_apellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_apellidoActionPerformed

    private void b_cedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_cedulaActionPerformed

    private void b_direccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_direccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_direccionActionPerformed

    private void b_telefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_telefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_telefonoActionPerformed

    private void b_fechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_fechaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_fechaActionPerformed

    private void b_sangreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_sangreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_sangreActionPerformed

    private void b_nombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_nombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_nombreActionPerformed

    private void guardar_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardar_bomberoActionPerformed
        crudsql crud_b = new crudsql();
        crud_b.insertar_bombero(b_distrito.getText(), b_nombre.getText(), b_apellido.getText(), b_cedula.getText(), b_telefono.getText(), b_direccion.getText(), b_fecha.getText(), b_sangre.getText());
        
       //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(false);
        b_apellido.setEditable(false);
        b_cedula.setEditable(false);
        b_telefono.setEditable(false);
        b_direccion.setEditable(false);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //FOCUS
        b_codigo.requestFocus();
        //BLANCO
        b_codigo.setText("");
        b_distrito.setText("");
        b_nombre.setText("");
        b_apellido.setText("");
        b_cedula.setText("");
        b_telefono.setText("");
        b_direccion.setText("");
        b_fecha.setText("");
        b_sangre.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_bombero.setEnabled(true); 
        
    }//GEN-LAST:event_guardar_bomberoActionPerformed

    private void b_distritoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_distritoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_distritoActionPerformed

    private void b_codigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_codigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b_codigoActionPerformed

    private void Boton_MenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Boton_MenuActionPerformed
        Menu objmenu = new Menu();
        objmenu.setVisible(true);
        objmenu.setLocationRelativeTo(null);
        this.setVisible(false);
    }//GEN-LAST:event_Boton_MenuActionPerformed

    private void buscar_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar_bomberoActionPerformed
        crudsql crud_b = new crudsql();
        bombero_variables var = new bombero_variables();
        crud_b.mostrar_bombero(b_codigo.getText());
        b_distrito.setText(var.getDistrito_id());
        b_nombre.setText(var.getBombero_nombre());
        b_apellido.setText(var.getBombero_apellido());
        b_cedula.setText(var.getBombero_cedula());
        b_telefono.setText(var.getBombero_telefono());
        b_direccion.setText(var.getBombero_direccion());
        b_fecha.setText(var.getBombero_fecha());
        b_sangre.setText(var.getBombero_sangre());
        
        //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(true);
        b_apellido.setEditable(true);
        b_cedula.setEditable(false);
        b_telefono.setEditable(true);
        b_direccion.setEditable(true);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //BOTON
        reinicio.setVisible(true);
        actualizar_bombero.setEnabled(true); 
        eliminar_bombero.setEnabled(true);
        guardar_bombero.setEnabled(false); 
        
        
        
    }//GEN-LAST:event_buscar_bomberoActionPerformed

    private void nuevo_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevo_bomberoActionPerformed
        
        //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(false);
        b_distrito.setEditable(false);
        b_nombre.setEditable(true);
        b_apellido.setEditable(true);
        b_cedula.setEditable(true);
        b_telefono.setEditable(true);
        b_direccion.setEditable(true);
        b_fecha.setEditable(true);
        b_sangre.setEditable(true);
        //FOCUS
        b_nombre.requestFocus();
        //NUEVO
        b_codigo.setText("");
        b_distrito.setText("13021");
        b_nombre.setText("");
        b_apellido.setText("");
        b_cedula.setText("");
        b_telefono.setText("");
        b_direccion.setText("");
        b_fecha.setText("");
        b_sangre.setText("");
        //BOTON
        reinicio.setVisible(true);
        guardar_bombero.setEnabled(true); 
        buscar_bombero.setEnabled(false); 
        actualizar_bombero.setEnabled(false); 
        eliminar_bombero.setEnabled(false); 
        
    }//GEN-LAST:event_nuevo_bomberoActionPerformed

    private void actualizar_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actualizar_bomberoActionPerformed
        crudsql crud_b = new crudsql();
        bombero_variables objvar = new bombero_variables();
        objvar.setBombero_id(b_codigo.getText());
        objvar.setBombero_nombre(b_nombre.getText());
        objvar.setBombero_apellido(b_apellido.getText());
        objvar.setBombero_cedula(b_cedula.getText());
        objvar.setBombero_telefono(b_telefono.getText());
        objvar.setBombero_direccion(b_direccion.getText());
        objvar.setBombero_fecha(b_fecha.getText());
        objvar.setBombero_sangre(b_sangre.getText());
        crud_b.actualizar_bombero(objvar.getBombero_id(), objvar.getBombero_nombre(), objvar.getBombero_apellido(), objvar.getBombero_cedula(), objvar.getBombero_telefono(), objvar.getBombero_direccion(), objvar.getBombero_fecha(), objvar.getBombero_sangre());
        
        //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(false);
        b_apellido.setEditable(false);
        b_cedula.setEditable(false);
        b_telefono.setEditable(false);
        b_direccion.setEditable(false);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //FOCUS
        b_codigo.requestFocus();
        //BOTONES
        reinicio.setVisible(false);
        guardar_bombero.setEnabled(false); 
        actualizar_bombero.setEnabled(false); 
        eliminar_bombero.setEnabled(false);
        //BLANCO
        b_codigo.setText("");
        b_distrito.setText("");
        b_nombre.setText("");
        b_apellido.setText("");
        b_cedula.setText("");
        b_telefono.setText("");
        b_direccion.setText("");
        b_fecha.setText("");
        b_sangre.setText("");
        
    }//GEN-LAST:event_actualizar_bomberoActionPerformed

    private void reinicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reinicioActionPerformed
        //Bloquear Acciones en TextFiel 
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(false);
        b_apellido.setEditable(false);
        b_cedula.setEditable(false);
        b_telefono.setEditable(false);
        b_direccion.setEditable(false);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //FOCUS
        b_codigo.requestFocus();
        //BLANCO
        b_codigo.setText("");
        b_distrito.setText("");
        b_nombre.setText("");
        b_apellido.setText("");
        b_cedula.setText("");
        b_telefono.setText("");
        b_direccion.setText("");
        b_fecha.setText("");
        b_sangre.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_bombero.setEnabled(true);
        guardar_bombero.setEnabled(false); 
        
    }//GEN-LAST:event_reinicioActionPerformed

    private void eliminar_bomberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar_bomberoActionPerformed
        crudsql crud_b = new crudsql();
        bombero_variables objvar = new bombero_variables();
        objvar.setBombero_id(b_codigo.getText());
        crud_b.eliminar_bombero(objvar.getBombero_id());

        //Bloquear Acciones en TextFiel
        b_codigo.setEditable(true);
        b_distrito.setEditable(false);
        b_nombre.setEditable(false);
        b_apellido.setEditable(false);
        b_cedula.setEditable(false);
        b_telefono.setEditable(false);
        b_direccion.setEditable(false);
        b_fecha.setEditable(false);
        b_sangre.setEditable(false);
        //FOCUS
        b_codigo.requestFocus();
        //BLANCO
        b_codigo.setText("");
        b_distrito.setText("");
        b_nombre.setText("");
        b_apellido.setText("");
        b_cedula.setText("");
        b_telefono.setText("");
        b_direccion.setText("");
        b_fecha.setText("");
        b_sangre.setText("");
        //BOTON
        reinicio.setVisible(false);
        buscar_bombero.setEnabled(true);
        guardar_bombero.setEnabled(false);

    }//GEN-LAST:event_eliminar_bomberoActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bomberos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bomberos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bomberos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bomberos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bomberos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Boton_Menu;
    private javax.swing.JButton actualizar_bombero;
    private javax.swing.JTextField b_apellido;
    private javax.swing.JTextField b_cedula;
    private javax.swing.JTextField b_codigo;
    private javax.swing.JTextField b_direccion;
    private javax.swing.JTextField b_distrito;
    private javax.swing.JTextField b_fecha;
    private javax.swing.JTextField b_nombre;
    private javax.swing.JTextField b_sangre;
    private javax.swing.JTextField b_telefono;
    private javax.swing.JButton buscar_bombero;
    private javax.swing.JButton eliminar_bombero;
    private javax.swing.JButton guardar_bombero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JButton nuevo_bombero;
    private javax.swing.JButton reinicio;
    // End of variables declaration//GEN-END:variables
}

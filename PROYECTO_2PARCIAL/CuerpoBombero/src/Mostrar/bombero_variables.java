
package Mostrar;

public class bombero_variables {
    
    private static String bombero_id;
    private static String distrito_id;
    private static String bombero_nombre;
    private static String bombero_apellido;
    private static String bombero_cedula;
    private static String bombero_telefono;
    private static String bombero_direccion;
    private static String bombero_fecha;
    private static String bombero_sangre;
    
    public String getBombero_id() {
        return bombero_id;
    }

    public void setBombero_id(String bombero_id) {
        this.bombero_id = bombero_id;
    }

    public String getDistrito_id() {
        return distrito_id;
    }

    public void setDistrito_id(String distrito_id) {
        this.distrito_id = distrito_id;
    }

    public String getBombero_nombre() {
        return bombero_nombre;
    }

    public void setBombero_nombre(String bombero_nombre) {
        this.bombero_nombre = bombero_nombre;
    }

    public String getBombero_apellido() {
        return bombero_apellido;
    }

    public void setBombero_apellido(String bombero_apellido) {
        this.bombero_apellido = bombero_apellido;
    }

    public String getBombero_cedula() {
        return bombero_cedula;
    }

    public void setBombero_cedula(String bombero_cedula) {
        this.bombero_cedula = bombero_cedula;
    }

    public String getBombero_telefono() {
        return bombero_telefono;
    }

    public void setBombero_telefono(String bombero_telefono) {
        this.bombero_telefono = bombero_telefono;
    }

    public String getBombero_direccion() {
        return bombero_direccion;
    }

    public void setBombero_direccion(String bombero_direccion) {
        this.bombero_direccion = bombero_direccion;
    }

    public String getBombero_fecha() {
        return bombero_fecha;
    }

    public void setBombero_fecha(String bombero_fecha) {
        this.bombero_fecha = bombero_fecha;
    }

    public String getBombero_sangre() {
        return bombero_sangre;
    }

    public void setBombero_sangre(String bombero_sangre) {
        this.bombero_sangre = bombero_sangre;
    }           
}


package Mostrar;

public class capacitacion_variables {
    
    private static String capacitacion_id;
    private static String bombero_id;
    private static String capacitacion_nombre;
    private static String capacitacion_fecha;
    private static String capacitacion_nota;

    public String getCapacitacion_id() {
        return capacitacion_id;
    }

    public void setCapacitacion_id(String capacitacion_id) {
        this.capacitacion_id = capacitacion_id;
    }

    public String getBombero_id() {
        return bombero_id;
    }

    public void setBombero_id(String bombero_id) {
        this.bombero_id = bombero_id;
    }

    public String getCapacitacion_nombre() {
        return capacitacion_nombre;
    }

    public void setCapacitacion_nombre(String capacitacion_nombre) {
        this.capacitacion_nombre = capacitacion_nombre;
    }

    public String getCapacitacion_fecha() {
        return capacitacion_fecha;
    }

    public void setCapacitacion_fecha(String capacitacion_fecha) {
        this.capacitacion_fecha = capacitacion_fecha;
    }

    public String getCapacitacion_nota() {
        return capacitacion_nota;
    }

    public void setCapacitacion_nota(String capacitacion_nota) {
        this.capacitacion_nota = capacitacion_nota;
    }
    
    

    
    
    
    }

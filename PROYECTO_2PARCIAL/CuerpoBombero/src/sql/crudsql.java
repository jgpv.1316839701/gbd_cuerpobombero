package sql;

import Mostrar.bombero_variables;
import Mostrar.capacitacion_variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;

public class crudsql extends conexionsql {

    java.sql.Statement st;
    ResultSet rs;
    bombero_variables var = new bombero_variables();
    capacitacion_variables cap = new capacitacion_variables();
    
    //==================================CRUD DE BOMBERO========================================
    public void insertar_bombero(String distrito, String nombre, String apellido, String cedula, String telefono, String direccion, String fecha, String sangre) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into bombero(distrito_id, bombero_nombre, bombero_apellido, bombero_cedula, bombero_telefono, bombero_direccion, bombero_fecha_contrato, bombero_tipo_sangre)"
                    +"values('" + distrito + "','" + nombre + "','" + apellido + "','" + cedula + "','" + telefono + "','" + direccion + "','" + fecha + "','" + sangre + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Los datos del bombero se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: Los datos no se guardaron correctamente" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void mostrar_bombero(String bombero_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from bombero where bombero_id='" + bombero_id + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setBombero_id(rs.getString("bombero_id"));
                var.setDistrito_id(rs.getString("distrito_id"));
                var.setBombero_nombre(rs.getString("bombero_nombre"));
                var.setBombero_apellido(rs.getString("bombero_apellido"));
                var.setBombero_cedula(rs.getString("bombero_cedula"));
                var.setBombero_telefono(rs.getString("bombero_telefono"));
                var.setBombero_direccion(rs.getString("bombero_direccion"));
                var.setBombero_fecha(rs.getString("bombero_fecha_contrato"));
                var.setBombero_sangre(rs.getString("bombero_tipo_sangre"));
            } else {
                JOptionPane.showMessageDialog(null, "No se encontro al Bombero", "Sin datos encontrados", JOptionPane.INFORMATION_MESSAGE);
                var.setBombero_id(rs.getString(""));
                var.setDistrito_id(rs.getString(""));
                var.setBombero_nombre(rs.getString(""));
                var.setBombero_apellido(rs.getString(""));
                var.setBombero_cedula(rs.getString(""));
                var.setBombero_telefono(rs.getString(""));
                var.setBombero_direccion(rs.getString(""));
                var.setBombero_fecha(rs.getString(""));
                var.setBombero_sangre(rs.getString(""));
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en programa " + e, "Error de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void actualizar_bombero(String bombero_id, String nombre, String apellido, String cedula, String telefono, String direccion, String fecha, String sangre) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update bombero set bombero_nombre='" + nombre + "',bombero_apellido='" + apellido + "',bombero_cedula='" + cedula + "',bombero_telefono='" + telefono + "',bombero_direccion='" + direccion + "',bombero_fecha_contrato='" + fecha + "',bombero_tipo_sangre='" + sangre + "' where bombero_id ='" + bombero_id + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Los datos del Bombero se actualizaron", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void eliminar_bombero(String bombero_id){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from bombero where bombero_id='"+bombero_id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro del Bombero Eliminado Correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al Eliminar el Registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
    
    //==================================CRUD DE CAPACITACION========================================
    public void insertar_capacitacion(String bombero_id, String capacitacion, String fecha, String nota) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into capacitacion(bombero_id, capacitacion_nombre, capacitacion_fecha, capacitacion_nota)"
                    +"values('" + bombero_id + "','" + capacitacion + "','" + fecha + "','" + nota +"');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Los datos de la capacitacion se Guardaron Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: Los datos no se guardaron correctamente" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void mostrar_capacitacion(String capacitacion_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from capacitacion where capacitacion_id='" + capacitacion_id + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                cap.setCapacitacion_id(rs.getString("capacitacion_id"));
                cap.setBombero_id(rs.getString("bombero_id"));
                cap.setCapacitacion_nombre(rs.getString("capacitacion_nombre"));
                cap.setCapacitacion_fecha(rs.getString("capacitacion_fecha"));
                cap.setCapacitacion_nota(rs.getString("capacitacion_nota"));                
            } else {
                JOptionPane.showMessageDialog(null, "No se encontro la capacitacion del Bombero", "Sin datos encontrados", JOptionPane.INFORMATION_MESSAGE);
                cap.setCapacitacion_id(rs.getString(""));
                cap.setBombero_id(rs.getString(""));
                cap.setCapacitacion_nombre(rs.getString(""));
                cap.setCapacitacion_fecha(rs.getString(""));
                cap.setCapacitacion_nota(rs.getString(""));
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en programa " + e, "Error de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void actualizar_capacitacion(String capacitacion_id, String bombero, String nombre, String fecha, String nota) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update capacitacion set bombero_id='" + bombero + "', capacitacion_nombre='" + nombre + "', capacitacion_fecha='" + fecha + "', capacitacion_nota='" + nota + "' where capacitacion_id ='" + capacitacion_id + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Los datos de la capacitacion se actualizaron", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void eliminar_capacitacion(String capacitacion_id){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from capacitacion where capacitacion_id='"+capacitacion_id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "La Capacitacion fue Eliminado Correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al Eliminar el Registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
}

const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


exports.onUserCreate = functions.firestore
    .document("Asignacion/{userId}")
    .onCreate(async (snap, context) => {
        const datos = snap.data();
        //Se realiza una consulta de los equipos asignados, cuando la Id de equipos y equipos sean iguales
        let query = db.collection("Asignacion").where("Bombero_id", "==", datos.Bombero_id).where("Equipos_id", "==", datos.Equipos_id);
        await query.get().then((snap) => {
            size = snap.size;
        });

        console.log(size);
        if (size > 1 ){            
            db.collection("Asignacion").doc(snap.id).delete()
            console.log("El bombero ya tiene asignado ese equipo");
        } else {
            console.log("El equipo se asigno");
        }
    });

